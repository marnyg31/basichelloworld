﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello User!");
            Console.WriteLine("I'm Marius!");
            Console.WriteLine("Whats your name");
            string line;
            line = Console.ReadLine();
            if (line.Length > 0)
            {
                Console.WriteLine("hello " + line + ", your name is " + line.Length + " long and starts with a " + line.Substring(0,1));
            }
            else
            {
                Console.WriteLine("you didn't type anything :'(");
            }

        }
    }
}
